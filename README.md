# qrcite
Generate QR Codes from .bib-files or given dois.

Example:

![Einstein1905 QR Code](https://codeberg.org/Meph/qrcite/media/branch/main/EinsteinUeberErzeugungund1905.png)

Inspired by https://github.com/nucleic-acid/namedropR.


## Installation

from pypi <https://pypi.org/project/qrcite/>

    pip install qrcite

from the git repository

    pip install https://codeberg.org/Meph/qrcite


## Usage after installation

    qrcite input.bib
    
    qrcite -d "<the doi to cite>"

    qrcite --help


## Examples

With text, red background

    qrcite inputfile.bib -c "#900000" -t png

QR Code only, as pdf, from doi only

    python -m qrcite -d "10.5281/zenodo.7545620" --color "#070" --style qrcode --type pdf


## Citation
Cite this project via <https://doi.org/10.5281/zenodo.7545620>:
![qrcite2023 QR Code](https://codeberg.org/Meph/qrcite/media/branch/main/qrcite2023.png)


## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.  

Follow me on <a rel="me" href="https://nerdculture.de/@M">Mastodon</a>.
